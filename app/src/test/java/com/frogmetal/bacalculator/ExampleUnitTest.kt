package com.frogmetal.bacalculator

import org.junit.Test

import org.junit.Assert.*

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {
    @Test
    fun standardDrinkCalculation_isCorrect() {
        assertEquals(1.75, Calculations.CalculateStandardDrinks("Australia", 500.0, 4.5, 1), 0.1)
        assertEquals(0.9, Calculations.CalculateStandardDrinks("Austria", 500.0, 4.5, 1), 0.1)
    }
}
