package com.frogmetal.bacalculator

object Calculations {
    private val countryStandardDrinkMap = mapOf( //map of grams of alcohol for a standard drink
        "Australia" to 10.0,
        "Austria" to 20.0,
        "Canada" to 13.6,
        "Denmark" to 12.0,
        "Finland" to 12.0,
        "France" to 10.0,
        "Germany" to 10.0,
        "Hong Kong" to 10.0,
        "Hungary" to 17.0,
        "Iceland" to 8.0,
        "Ireland" to 10.0,
        "Italy" to 10.0,
        "Japan" to 19.75
    )
    private const val volumetricMassDensity = 789.24 // g/L
    val countries: List<String> get() = countryStandardDrinkMap.keys.toList()

    fun CalculateStandardDrinks(country: String, volume: Double, abv: Double, quantity: Int): Double {
        val TotalGrams = quantity * calculatePureAlcoholMass(volume / 1000, abv / 100)
        return (TotalGrams / countryStandardDrinkMap.getValue(country))
    }

    private fun calculatePureAlcoholMass(volume: Double, abv: Double) = volume * abv * volumetricMassDensity
}