package com.frogmetal.bacalculator

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView

class MainActivity : AppCompatActivity() {
    val _drinkList = mutableListOf<String>()
    val btnAddDrink: Button = findViewById(R.id.bt_add_drink) as Button;
    val btnClear: Button = findViewById(R.id.btClear) as Button
    val country = "Australia"
    val quantity: TextView = findViewById(R.id.inputQuantity) as TextView
    val volume: TextView = findViewById(R.id.inputVolume) as TextView
    val percent: TextView = findViewById(R.id.inputAbv) as TextView
    val showDrinks: TextView = findViewById(R.id.viewDrinkList) as TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnAddDrink.setOnClickListener {
            addDrink()
        }


        btnAddDrink.setOnClickListener {
            clearList()
        }

    }

    private fun addDrink(){
        val numberOfDrinks = Calculations.CalculateStandardDrinks(country, volume.text.toString().toDouble(), percent.text.toString().toDouble(), quantity.text.toString().toInt())

    }

    private fun clearList(){
        showDrinks.text = ""
    }
}
